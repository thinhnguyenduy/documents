
.. Created by Rubikin Team.
.. ========================
.. Date: 2014-06-15
.. Time: 05:47:05 AM

.. Author: Vinh Trinh <vinhtrinh@rubkin.com>

.. This file is part of Rubikin

.. Question? Come to our website at http://rubikin.com
.. For the full copyright and license information, please view the LICENSE
.. file that was distributed with this source code.


PHP Coding Standard
===================

This document extended `PSR <http://www.php-fig.org/>`__, `Symfony Coding Standards <http://symfony.com/doc/current/contributing/code/standards.html>`__ and **Based Coding Standards**.


Common
------

PHP files and PHP code **MUST** follow **Based Coding Standards**.


Structure
~~~~~~~~~

- Define one class per file.
- Declare class properties before methods
- Declare public methods first, then protected ones and finally private ones. The exceptions to this rule are the class constructor and the setUp and tearDown methods of PHPUnit tests, which should always be the first methods to increase readability.
- Exception message strings should be concatenated using ``sprintf``.


Naming Conventions
~~~~~~~~~~~~~~~~~~

- Use ``camelCase``, **NOT** ``underscores``, for variable, function and method names, arguments;
- Use ``underscores`` for option names and parameter names;
- Use namespaces for all classes;
- Prefix abstract classes with ``Abstract``. Please note some early Symfony classes **do not** follow this convention and have not been renamed for backward compatibility reasons. However all new abstract classes **must** follow this naming convention;
- Suffix interfaces with ``Interface``;
- Suffix traits with ``Trait``;
- Suffix exceptions with ``Exception``;
- Use alphanumeric characters and underscores for file names;
- For type-hinting in PHPDocs and casting, use ``boolean`` (instead of ``bool`` or ``Boolean``), ``integer`` (instead of ``int``), ``float`` (instead of ``double`` or ``real``)


Documentation
~~~~~~~~~~~~~

- Add PHPDoc blocks for **ALL** ``classes``, ``methods``, ``functions`` and ``properties``;
- Align parameter, property names
- Align tag comments
- Add a single blank line before first tag
- Add a single blank line around parameters
- Omit the ``@return`` tag if the method does not return anything;
- Use ``@return self`` for method returned ``$this``
- The ``@package`` and ``@subpackage`` annotations are **NOT** used.

.. code-block:: php

    /**
     * @param string $dummy Some argument description
     * @param array  $options
     *
     * @return string|null Transformed input
     *
     * @throws RuntimeException
     */
    public function transformText($dummy, array $options = array())
    {
        $mergedOptions = array_merge(
            array(
                'some_default' => 'values',
                'another_default' => 'more values',
            ),
            $options
        );

        if (true === $dummy) {
            return;
        }

        if ('string' === $dummy) {
            if ('values' === $mergedOptions['some_default']) {
                return substr($dummy, 0, 5);
            }

            return ucwords($dummy);
        }

        throw new RuntimeException(sprintf('Unrecognized dummy option "%s"', $dummy));
    }

Spaces
------

Add a single space before parentheses ``(``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``if`` parentheses:

.. code-block:: php

    if (null === $someVar) {
        // Do something...
    }

``for`` parentheses:

.. code-block:: php

    for ($i = 0; $i < 10; $i++) {
        // Do something...
    }

``foreach`` parentheses:

.. code-block:: php

    foreach (range(0, 10) as $number) {
        // Do something...
    }

``while`` parentheses:

.. code-block:: php

    while (true === $someVar) {
        // Do something...
    }

``switch`` parentheses:

.. code-block:: php

    switch ($someVar) {
        case 0:
            // Do something...
            break;
        case 1:
            // Do something...
            break;
        default:
            // Do something...
    }

``catch`` parentheses:

.. code-block:: php

    try {
        // Some codes...
    } catch (AnyException $e) {
        // Do something...
    }


Add a single space around operators (``+``, ``-``, ``*``, ``/``, ``&``, ``!``,...)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Assignment operators
    + Arithmetic: ``+=``, ``-=``, ``*=``, ``/=``, ``%=``
    + String: ``.=``
    + Bitwise: ``&=``, ``|=``, ``^=``, ``<<=``, ``>>=``
- Logical operators ``&&``, ``||``, ``!!``, ``!``
- Equality operators ``==``, ``!=``
- Relational operators ``<``, ``>``, ``<=``, ``>=``
- Bitwise operators ``&``, ``|``, ``^``
- Additive operators ``+``, ``--``
- Multiplicative operators ``*``, ``/``, ``%``
- Shift operators ``<<``, ``>>``, ``>>>``
- Concatenation ``.``
- In ternary operator ``?:``

.. code-block:: php

    $total = 105;

    echo 'The total value ' . $total . ' is an ' . (0 === $total % 2 ? 'even' : 'odd') . ' number';

    for ($i = 1; $i <= 105; $i++) {
        if (0 === $i % 21) {
            $msg = 'RubikIntegration';
        } else if (0 === $i % 3) {
            $msg = 'Rubik';
        } else if (0 === $i % 7) {
            $msg = 'Integration';
        } esle {
            $msg = $i;
        }

        echo $msg . '<br>';
    }

Add a single space before left brace
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


``if``, ``else if``, ``else`` left brace

.. code-block:: php

    if (is_array($someVar)) {
        // Do something...
    } else if (is_string($someVar)) {
        // Do something...
    } else {
        // Do something else...
    }

``for``, ``foreach`` left brace

.. code-block:: php

    for ($i = 0; $i < 10; $i++) {
        // Do something...
    }

    foreach (range(0, 10) as $number) {
        // Do something...
    }

``while``, ``do ... while`` left brace

.. code-block:: php

    while (true === $someVar) {
        // Do something...
    }

    do {
        // Do something...
    } while (true === $someVar);

``switch`` left brace

.. code-block:: php

    switch ($someVar) {
        case 0:
            // Do something...
            break;
        case 1:
            // Do something...
            break;
        default:
            // Do something...
    }

``try``, ``catch``, ``finally`` left brace

.. code-block:: php

    try {
        // Some codes...
    } catch (AnyException $e) {
        // Do something...
    } finally {
        // Clean things up...
    }


Add a single space before keyword
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``else``, ``while``, ``catch``, ``finally`` keywords

.. code-block:: php

    $arr = range(-5, 5);

    do {
        $number = array_pop($arr);

        try {
            $result = 10 / $number;
        } catch (Exception $e) {
            echo 'Caught exception: ' .  $e->getMessage() . "\n";
        } finally {
            // Do something...
        }

        if (is_empty($arr)) {
            break;
        } else {
            continue;
        }
    } while (true);


Add a single space after comma ``,`` and semicolon ``;``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: php

    $elements = array(1, 3, 6, 9);

    echo sum($slements);

    function sum(array $elements)
    {
        $total = 0;

        for ($i = 0; $i < count($elements); $i++) {
            $total += $elements[$i];
        }

        return $total;
    }


Wrapping and Braces
---------------------


General
~~~~~~~

- **DO NOT** use comment at first column

.. code-block:: php

    function divide($x, $y)
    {
    //    comments at first column are invalid
    //    more than one space after inline comments "//" are invalid too
    //    return $x / $y;

        try {
            // comments at current indent level are valid
            // add a single space after inline comments "//"
            // return $x / $y;

            $result = $x / $y;
        } catch (Exception $e) {
            throw $e;
        }

        return $result;
    }

- **DO NOT** use control statement in one line

.. code-block:: php

    // these one line statement are invalid
    if (true === $oneLineStatement) echo 'invalid'; else echo 'valid';
    if (true === $oneLineStatement) { echo 'invalid'; } else { echo 'valid'; }

    // this statement is valid
    if (true !== $oneLineStatement) {
        echo 'valid';
    } else {
        echo 'invalid';
    }

- **DO NOT** use simple function, method in one line

.. code-block:: php

    // this function defination is invalid
    function isInlineFunction() { return true; }

    // this function defination is valid
    function isInlineFunction()
    {
        return false;
    }


Braces placement
~~~~~~~~~~~~~~~~

Add braces placement at the next line in **class** and **function** declaration.
Add braces placement at the end of line in orther case.

.. code-block:: php

    class Foo
    {
        protected $number;

        public function setNumber($number)
        {
            if ( ! is_numeric($number)) {
                throw new InvalidAgumentException('Exception message');
            }

            $this->number = $number;

            return $this;
        }
    }


Extends / Implements list
~~~~~~~~~~~~~~~~~~~~~~~~~

Place extends or (and) implements list at the same line if posible. But this list can be wrap if it's too long.

When wrapped, it **MUST** wrapped by ``extends`` and ``implements`` keyword and every class**MUST** declaration in a single line.

.. code-block:: php

    // single line declaration example
    class Foo extends AbstractFoo implements FooInterface
    {
        // class body...
    }

    // multi lines declaration example
    class Foo
        extends AbstractFoo
        implements
            FooOneInterface,
            FooTwoInterface,
            FooThreeInterface
    {
        // class body
    }


Function declaration parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The parameters with default value **MUST** placed at the end of list.

The function parameters can be wrap if it's too long.

When wrapped, every parameter **MUST** declaration in a single line.

.. code-block:: php

    // single line declaration example
    function Foo($barOne, $barTwo, $barThree = null)
    {
        // function body...
    }

    // multi lines declaration example
    function Foo(
        BarOneInterface $barOne,
        BarTwoInterface $barTwo,
        array $barThree,
        array $barFour,
        $barFive = true,
        $barSix = null
    ) {
        // function body...
    }


Function call arguments
~~~~~~~~~~~~~~~~~~~~~~~

The function parameters can be wrap if it's too long.

When wrapped, every parameter **MUST** declaration in a single line.

.. code-block:: php

    $foo = BarOne(
        $barOneInstance,
        $barTwoInstance,
        $barThreeArray,
        BarTwo(
            $barOneInstance,
            $barTwoInstance,
            $barThreeArray,
            $barFourArray
        ),
        true,
        false
    );


Chained method call
~~~~~~~~~~~~~~~~~~~

The chained methods can be wrap if it's too long.

When wrapped, every method **MUST** declaration in a single line. The semicolon ``;`` **MUST** placed at same indent level;

.. code-block:: php

    $queryBuilder
        ->select(array('foo', 'bar'))
        ->where($queryBuilder->expr()->eq('o.foo', ':foo'))
        ->andWhere($queryBuilder->expr()->eq('o.bar', ':bar'))
        ->setParameters(array(
            'foo' => 'fooValue',
            'bar' => 'barValue'
        ))
        ->orderBy('o.foo', 'ASC')
    ;


Statements
~~~~~~~~~~

Force braces for all statements.

All statements can be wrap if it's too long.

The keyword ``else if`` **MUST** be used instead of ``elseif``. ``else`` and ``else if`` are on the same line as the closing brace from the earlier body

.. code-block:: php

    // invalid statements declaration
    if ($foo) return true; else return false;

    if ($foo) return true;
    else return false;

    if ($foo)
        return true;
    else
        return false;

    // valid statements declaration
    if ($simple) {
        return 'MUST use braces';
    } else if ($compound) {
        // if you want to comment for this statement..
        // write here...
        return 'MUST use braces';
    }
    // **do not** break and write here...
    else {
        return 'MUST use braces';
    }

    foreach (array(
        1 => 'Sun',
        2 => 'Mon',
        3 => 'Tue',
        4 => 'Wed',
        5 => 'Thu',
        6 => 'Fri',
        7 => 'Sat'
    ) as $index => $element) {
        // Do something...
    }


Blank Lines
-----------

All PHP files **MUST** end with a single blank line.

There **MUST** be one blank line around the ``namespace``, block of ``use``, ``class`` and ``method`` declarations.

There **MUST** be one ``use`` keyword per declaration.

Add a blank line before ``return`` statements, unless the ``return`` is alone inside a statement-group (like an ``if`` statement);

There **SHOULD** be group and sort by name block of ``use`` declarations.

.. code-block:: php

    <?php
    /**
     * Created by Rubikin Team.
     * ========================
     * Date: 2014-06-17
     * Time: 03:31:46 AM
     *
     * @author Vinh Trinh <vinhtrinh@rubikin.com>
     *
     * This file is a part of Rubikin
     *
     * Question? Come to our website at http://rubikin.com
     * For the full copyright and license information, please view the LICENSE
     * file that was distributed with this source code.
     */

    namespace Rubikin\ExampleNamespace;

    use DateTime;
    use InvalidAgumentException;

    use Rubikin\ExampleNamespace\Bar;
    use Rubikin\ExampleNamespace\Foo as BaseFoo;

    use AnotherVendor\Namespace as Alias;
    use Vendor\Namespace;

    /**
     * Foo class description
     */
    class Foo extends BaseFoo
    {
        /**
         * Bar property description
         *
         * @var DateTime
         */
        protected $bar;

        /**
         * Bar getter description
         *
         * @return DateTime
         */
        public function getBar()
        {
            return $this->bar;
        }

        /**
         * Bar setter description
         *
         * @param DateTime $bar The bar value description
         *
         * @return self
         */
        public function setBar(DateTime $bar)
        {
            $this->bar = $bar;

            return $this;
        }
    }
    // <~ this is a single blank line in the end, without this comment
